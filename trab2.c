/* Mario Augusto Mota Martins */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Delta{
	char caracter;
	int flag, valor; // a flag é usada na inicializacao do delta1 para checar se já foi atribuido um valor a um determinado char
};
typedef struct Delta *delta;

int main(){
	int txtSize,patternLenght,flagAux,d1Size,i,j,c,d,*ocorrencias;
	char *text,*pattern,menu;
	
	scanf("%d", &txtSize);
	
	text = (char *)malloc(sizeof(char)*txtSize);
	pattern = (char *)malloc(sizeof(char)*txtSize);
	ocorrencias = (int *)malloc(sizeof(int)*txtSize); //vetor para armazenar ocorrencias do padrao no texto
	
	for(i = 0; i<txtSize ; i++)
	{
		ocorrencias[i] = -1;
		pattern[i] = '-';
	}
	scanf (" %[^\n]s", text);
	scanf (" %[^\n]s", pattern);
	
	for (i = 0; i<txtSize ; i++)
		if(((pattern[i]<'a')||(pattern[i]>'z'))&&(pattern[i]!='.')&&(pattern[i]!=',')&&(pattern[i]!=' '))
			break;
	patternLenght=i;
	
	//delta1 ---------------------------
	d1Size=29; //tamanho do conjunto de todas as lowercaseletters (26) + 3 chars
	delta d1[d1Size];
	
	for(i = 'a'; i<='z' ; i++)
	{
		d1[i-'a'] = (delta)malloc(sizeof(struct Delta));
		d1[i-'a']->caracter = i;
		d1[i-'a']->flag = 0;
		d1[i-'a']->valor = patternLenght;
	}
	i= i-'a'; //26
	d1[i] = (delta)malloc(sizeof(struct Delta));
	d1[i]->caracter = '.';
	d1[i]->flag = 0;
	d1[i]->valor = patternLenght;
	i++; //27
	d1[i] = (delta)malloc(sizeof(struct Delta));
	d1[i]->caracter = ',';
	d1[i]->flag = 0;
	d1[i]->valor = patternLenght;
	i++; //28
	d1[i] = (delta)malloc(sizeof(struct Delta));
	d1[i]->caracter = ' ';
	d1[i]->flag = 0;
	d1[i]->valor = patternLenght;
	
	for (i=patternLenght-1;i>=0;i--){ //inicializa delta1 com o tamanho do pattern pois é apenas o que está sendo utilizado até então
		switch (pattern[i])
		{
			case '.':
				if(d1[d1Size-3]->flag==0)
				{
					d1[d1Size-3]->flag = 1;
					d1[d1Size-3]->valor = patternLenght-(i+1);
				}
				break;
			case ',':
				if(d1[d1Size-2]->flag==0)
				{
					d1[d1Size-2]->flag = 1;
					d1[d1Size-2]->valor = patternLenght-(i+1);
				}
				break;
			case ' ':
				if(d1[d1Size-1]->flag==0)
				{
					d1[d1Size-1]->flag = 1;
					d1[d1Size-1]->valor = patternLenght-(i+1);
				}
				break;
			default:
				if(d1[pattern[i]-'a']->flag==0)
				{
					d1[pattern[i]-'a']->flag = 1;
					d1[pattern[i]-'a']->valor = patternLenght-(i+1);
				}
				break;
		}
	}
	//fim delta1 -----------------------
	
	//delta2 ---------------------------
	delta d2[patternLenght];
	d2[patternLenght-1] = (delta)malloc(sizeof(struct Delta));
	d2[patternLenght-1]->caracter=pattern[patternLenght-1]; //Adiciona o ultimo char que tem sempre valor 1
	d2[patternLenght-1]->valor=1;
	for (i=patternLenght-2;i>=0;i--){
		d2[i] = (delta)malloc(sizeof(struct Delta));
		d2[i]->caracter=pattern[i];
		
		for(j = i; j>=-patternLenght+1 ; j--)
		{
			if (((pattern[i+1]==pattern[j])&&(pattern[i]!=pattern[j-1]))||(j<0))
			{
				d=j;
				flagAux=1;
				for(c = i+1; c<patternLenght ; c++)
				{
					if((d>=0)&&(pattern[c]!=pattern[d]))
					{
						flagAux=0;
						break;
					}
					else
						d++;
				}
				if(flagAux==1)
				{
					d2[i]->valor= patternLenght - (j+1) +1;
					break;
				}
			}
		}
	}
	//fim delta2 -----------------------
	
	//buscaPadrao ----------------------
	j=patternLenght-1;
	i=j;
	d=0;
	while(j<txtSize)
	{
		if(text[j]==pattern[i])
		{
			if(i==0)
			{
				ocorrencias[d] = j+1;
				j+=d2[i]->valor;
				i=patternLenght-1;
				d++;
			}
			else
			{
				i--;
				j--;				
			}
		}
		else
		{
			switch (text[j])
			{
				case '.':
					c = d1Size-3;
					break;
				case ',':
					c = d1Size-2;
					break;
				case ' ':
					c = d1Size-1;
					break;
				default:
					c=text[j]-'a';
					break;
			}
			
			if(d1[c]->valor > d2[i]->valor)
				j+=d1[c]->valor;
			else
				j+=d2[i]->valor;
			i=patternLenght-1;
		}
	}
	
	while(1){
		scanf("%c", &menu);
		switch (menu){
			case 's':
				for (i=0; i<d; i++)
					printf("%d\n",ocorrencias[i]);
				break;
			case 'u':
				//imprimeDelta1
				printf("Tabela Delta 1:\n");
				for (i=0; i<d1Size-1; i++)
					printf("%c: %d\n",d1[i]->caracter,d1[i]->valor);
				printf("\'%c\': %d\n",d1[i]->caracter,d1[i]->valor);
				break;
			case 'd':
				//imprimeDelta2
				printf("Tabela Delta 2:\n");
				for (i=0; i<patternLenght; i++)
				{
					if(d2[i]->caracter!=' ')
						printf("%c: %d\n",d2[i]->caracter,d2[i]->valor);
					else
						printf("\'%c\': %d\n",d2[i]->caracter,d2[i]->valor);
				}
				break;
			case 'e':
				return 0;
		}
	}
	return 0;
}